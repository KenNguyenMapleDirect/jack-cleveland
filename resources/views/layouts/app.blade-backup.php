<!DOCTYPE html>
<html
    class=" js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers no-applicationcache svg inlinesvg smil svgclippaths"
    lang="en-US" style="overflow: auto; zoom: 1">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Force IE to use the latest rendering engine available -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!--Google Fonts Below-->
    <link rel="stylesheet" href="/HeaderFooterAssets/phv1gon.css">

    <!-- Icons & Favicons -->
    <link rel="icon" href="https://ballysac.com/wp-content/themes/BallysAtlanticCityCasino/favicon.png">
    <link href="https://ballysac.com/wp-content/themes/BallysAtlanticCityCasino/assets/images/apple-icon-touch.png"
          rel="apple-touch-icon">
    <!--[if IE]>
    <link rel="shortcut icon" href="https://ballysac.com/wp-content/themes/BallysAtlanticCityCasino/favicon.ico">
    <![endif]-->
    <meta name="msapplication-TileColor" content="#f01d4f">
    <meta name="msapplication-TileImage"
          content="https://ballysac.com/wp-content/themes/BallysAtlanticCityCasino/assets/images/win8-tile-icon.png">
    <meta name="theme-color" content="#121212">

    <link rel="pingback" href="https://ballysac.com/xmlrpc.php">

    <title>Bally's Atlantic City Hotel and Casino</title>
    <script type="text/javascript" src="{{asset('flipbook_assets/js/jquery.min.1.7.js')}}"></script>
    <link rel="dns-prefetch" href="https://maxcdn.bootstrapcdn.com/">
    <link rel="dns-prefetch" href="https://s.w.org/">
    <link rel="alternate" type="application/rss+xml" title="Bally&#39;s Atlantic City Hotel and Casino » Feed"
          href="https://ballysac.com/feed/">
    <link rel="alternate" type="application/rss+xml" title="Bally&#39;s Atlantic City Hotel and Casino » Comments Feed"
          href="https://ballysac.com/comments/feed/">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="/HeaderFooterAssets/wp-emoji-release.min.js" type="text/javascript" defer=""></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel="stylesheet" id="wp-block-library-css" href="/HeaderFooterAssets/style.min.css" type="text/css"
          media="all">
    <link rel="stylesheet" id="normalize-css-css" href="/HeaderFooterAssets/normalize.min.css" type="text/css"
          media="all">
    <link rel="stylesheet" id="foundation-css-css" href="/HeaderFooterAssets/foundation.min.css" type="text/css"
          media="all">
    <link rel="stylesheet" id="site-css-css" href="/HeaderFooterAssets/style.css" type="text/css" media="all">
    <link rel='stylesheet' id='font-awesome-css'
          href='{{ asset('assets/font-awesome/4.3.0/css/font-awesome.min40df.css?ver=5.6') }}' type='text/css'
          media='all'/>
    <link rel="https://api.w.org/" href="https://ballysac.com/wp-json/">
    <link rel="alternate" type="application/json" href="https://ballysac.com/wp-json/wp/v2/pages/19">
    <link rel="alternate" type="application/json+oembed"
          href="https://ballysac.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fballysac.com%2F">
    <link rel="alternate" type="text/xml+oembed"
          href="https://ballysac.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fballysac.com%2F&amp;format=xml">
    <style type="text/css" id="wp-custom-css">
        .page-title {
            display: none;
        }

        .column {
            float: left;
            width: 50%;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }
        .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('https://ballysac.maplewebservices.com/loading.gif') 50% 50% no-repeat rgb(255, 255, 255);
        }
    </style>

    <meta class="foundation-data-attribute-namespace">
    <meta class="foundation-mq-xxlarge">
    <meta class="foundation-mq-xlarge-only">
    <meta class="foundation-mq-xlarge">
    <meta class="foundation-mq-large-only">
    <meta class="foundation-mq-large">
    <meta class="foundation-mq-medium-only">
    <meta class="foundation-mq-medium">
    <meta class="foundation-mq-small-only">
    <meta class="foundation-mq-small">
    <style></style>
    <meta class="foundation-mq-topbar">
    <link href="/weekender_assets/css/modal_slick.css" rel="stylesheet">
    <link href="/weekender_assets/css/modal_slick-theme.css" rel="stylesheet">
    <link href="/weekender_assets/css/modal_lightbox.min.css" rel="stylesheet">
    <link href="/weekender_assets/css/modal_style.css" rel="stylesheet">
    <script type="text/javascript"
            src="{{asset('flipbook_assets/js/modernizr.2.5.3.min.js')}}"></script>
</head>
<body class="home page-template page-template-front-page page-template-front-page-php page page-id-19">
<div id="flipbook-section">
<div id="flipbook-section1">
<div id="flipbook-section2">
<div id="flipbook-section3">
    <div id="weekender-section">

    <div class="off-canvas-wrap" data-offcanvas style="background: linear-gradient(180deg, rgba(225,37,27,1) 2%, rgba(176,37,37,1) 90%);">
    <div class="inner-wrap">
        <div id="container">
            <header class="header" role="banner">
                <div id="inner-header" class="row clearfix">
                    <div class="show-for-medium-up contain-to-grid">
                        <nav class="top-bar" data-topbar="" style="max-width: 71.25rem;">
                            <div class="row" style="height: 100%">
                                <div class="col-sm-3">
                                    <ul class="title-area">
                                        <!-- Title Area -->
                                        <li class="name">
                                            <a href="https://ballysac.com/" rel="nofollow">
                                                <img src="/HeaderFooterAssets/BAC_Logo_blk.png" alt="bally&#39;s logo"></a>
                                        </li>
                                    </ul>

                                </div>
                                <div class="col-sm-9">
                                    <section class="top-bar-section">
                                        <ul id="menu-main-navigation" class="top-bar-menu" style="bottom: 0; right: 0; position: absolute;width: 850px;">
                                            <li class="divider"></li>
                                            <li id="menu-item-1831"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-1831 not-click">
                                                <a href="https://ballysac.com/casino/">Casino</a>
                                                <ul class="sub-menu dropdown">
                                                    <li class="title back js-generated"><h5><a
                                                                href="javascript:void(0)">Back</a></h5></li>
                                                    <li class="parent-link hide-for-medium-up"><a
                                                            class="parent-link js-generated"
                                                            href="https://ballysac.com/casino/">Casino</a></li>
                                                    <li id="menu-item-5024" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5024"><a href="https://ballysac.maplewebservices.com/admin/login">Player Portal</a></li>
                                                    <li id="menu-item-1081"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1081">
                                                        <a href="https://ballysac.com/casino/promotions/">Promotions</a></li>
                                                    <li id="menu-item-79"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79">
                                                        <a href="https://ballysac.com/casino/slots-and-video-poker/">Slots and
                                                            Video Poker</a></li>
                                                    <li id="menu-item-1762"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1762">
                                                        <a href="https://ballysac.com/casino/table-games/">Table Games</a></li>
                                                    <li id="menu-item-4998"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4998">
                                                        <a href="https://ballysac.com/casino/players-club/">Players Club</a>
                                                    </li>
                                                    <li id="menu-item-2035"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2035">
                                                        <a href="https://ballysac.com/casino/casino-credit/">Casino Credit</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="divider"></li>
                                            <li id="menu-item-1767"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-1767 not-click">
                                                <a href="https://ballysac.com/hotel/">Hotel</a>
                                                <ul class="sub-menu dropdown">
                                                    <li class="title back js-generated"><h5><a
                                                                href="javascript:void(0)">Back</a></h5></li>
                                                    <li class="parent-link hide-for-medium-up"><a
                                                            class="parent-link js-generated" href="https://ballysac.com/hotel/">Hotel</a>
                                                    </li>
                                                    <li id="menu-item-4987"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4987">
                                                        <a target="_blank" rel="noopener" href="https://ballysac.reztrip.com/">Book
                                                            Your Stay</a></li>
                                                </ul>
                                            </li>
                                            <li class="divider"></li>
                                            <li id="menu-item-4844"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-4844 not-click">
                                                <a href="https://ballysac.com/sportsbook/">Sportsbook</a>
                                                <ul class="sub-menu dropdown">
                                                    <li class="title back js-generated"><h5><a
                                                                href="javascript:void(0)">Back</a></h5></li>
                                                    <li class="parent-link hide-for-medium-up"><a
                                                            class="parent-link js-generated"
                                                            href="https://ballysac.com/sportsbook/">Sportsbook</a></li>
                                                    <li id="menu-item-4970"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4970">
                                                        <a href="https://ballysac.com/sportsbook-house-rules/">FanDuel
                                                            Sportsbook House Rules</a></li>
                                                </ul>
                                            </li>
                                            <li class="divider"></li>
                                            <li id="menu-item-4845"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-4845 not-click">
                                                <a href="https://ballysac.com/restaurants/">Restaurants</a>
                                                <ul class="sub-menu dropdown">
                                                    <li class="title back js-generated"><h5><a
                                                                href="javascript:void(0)">Back</a></h5></li>
                                                    <li class="parent-link hide-for-medium-up"><a
                                                            class="parent-link js-generated"
                                                            href="https://ballysac.com/restaurants/">Restaurants</a></li>
                                                    <li id="menu-item-4846"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4846">
                                                        <a href="https://ballysac.com/restaurants/buca-di-beppo/">Buca Di
                                                            Beppo</a></li>
                                                    <li id="menu-item-4847"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4847">
                                                        <a href="https://ballysac.com/restaurants/dunkin-donuts/">Dunkin’
                                                            Donuts</a></li>
                                                    <li id="menu-item-4849"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4849">
                                                        <a href="https://ballysac.com/restaurants/guy-fieris-chophouse/">Guy
                                                            Fieri’s Chophouse</a></li>
                                                    <li id="menu-item-4850"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4850">
                                                        <a href="https://ballysac.com/restaurants/harrys-oyster-bar/">Harry’s
                                                            Oyster Bar</a></li>
                                                    <li id="menu-item-4851"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4851">
                                                        <a href="https://ballysac.com/restaurants/johnny-rockets/">Johnny
                                                            Rockets</a></li>
                                                     <li id="menu-item-4852"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4852">
                                                        <a href="https://ballysac.com/restaurants/noodle-village/">Noodle
                                                            Village</a></li>
                                                    <li id="menu-item-4854"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4854">
                                                        <a href="https://ballysac.com/restaurants/walts-primo-pizza/">Walt’s
                                                            Primo Pizza</a></li>
                                                    <li id="menu-item-4853"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4853">
                                                        <a href="https://ballysac.com/restaurants/sack-o-subs/">Sack O’ Subs</a>
                                                    </li>
                                                    <li id="menu-item-5011"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5011">
                                                        <a href="https://ballysac.com/restaurants/sportsbook-lounge/">Sportsbook
                                                            Lounge</a></li>
                                                </ul>
                                            </li>
                                            <li class="divider"></li>
                                            <li id="menu-item-4962"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4962">
                                                <a href="https://ballysac.com/contact/">Contact</a></li>
                                            <li class="divider"></li>
                                            <li id="menu-item-4961"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4961">
                                                <a href="https://ballysac.com/careers/">Careers</a></li>
                                            @if ((Auth::User()))
                                                <li id="menu-item-5012" onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5011">
                                                    <a href="/logout">Logout</a></li>
                                                <form id="logout-form" action="/logout" method="POST"
                                                      style="display: none;">
                                                    @csrf
                                                </form>
                                                @else
                                            <li id="menu-item-5012"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5011">
                                                <a href="/login">Login</a></li>
                                            @endif
                                        </ul>
                                    </section>

                                </div>
                            </div>
                        </nav>
                    </div>

                    <div class="sticky fixed show-for-small">
                        <nav class="tab-bar">
                            <section class="middle tab-bar-section" style="width: 300px;">
                                <a href="https://ballysac.com/" rel="nofollow"><h1 class="title">Bally's Casino Hotel</h1></a>
                            </section>
                            <section class="left-small">
                                <a href="#" class="left-off-canvas-toggle menu-icon"><span><i
                                            class="fa fab fa-bars"></i></span></a>
                            </section>
                        </nav>
                    </div>

                    <aside class="left-off-canvas-menu show-for-small-only">
                        <ul class="off-canvas-list">
                            <a href="https://ballysac.com/"><span class="home-link">Home</span></a>
                            <ul id="menu-main-navigation-1" class="off-canvas-list">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1831">
                                    <a href="https://ballysac.com/casino/">Casino</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5024"><a href="https://ballysac.maplewebservices.com/admin/login">Player Portal</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1081">
                                            <a href="https://ballysac.com/casino/promotions/">Promotions</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79">
                                            <a href="https://ballysac.com/casino/slots-and-video-poker/">Slots and Video
                                                Poker</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1762">
                                            <a href="https://ballysac.com/casino/table-games/">Table Games</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4998">
                                            <a href="https://ballysac.com/casino/players-club/">Players Club</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2035">
                                            <a href="https://ballysac.com/casino/casino-credit/">Casino Credit</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1767">
                                    <a href="https://ballysac.com/hotel/">Hotel</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4987">
                                            <a target="_blank" rel="noopener" href="https://ballysac.reztrip.com/">Book
                                                Your Stay</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4844">
                                    <a href="https://ballysac.com/sportsbook/">Sportsbook</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4970">
                                            <a href="https://ballysac.com/sportsbook-house-rules/">FanDuel Sportsbook
                                                House Rules</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4845">
                                    <a href="https://ballysac.com/restaurants/">Restaurants</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4846">
                                            <a href="https://ballysac.com/restaurants/buca-di-beppo/">Buca Di Beppo</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4847">
                                            <a href="https://ballysac.com/restaurants/dunkin-donuts/">Dunkin’ Donuts</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4849">
                                            <a href="https://ballysac.com/restaurants/guy-fieris-chophouse/">Guy Fieri’s
                                                Chophouse</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4850">
                                            <a href="https://ballysac.com/restaurants/harrys-oyster-bar/">Harry’s Oyster
                                                Bar</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4851">
                                            <a href="https://ballysac.com/restaurants/johnny-rockets/">Johnny
                                                Rockets</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4852">
                                            <a href="https://ballysac.com/restaurants/noodle-village/">Noodle
                                                Village</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4854">
                                            <a href="https://ballysac.com/restaurants/walts-primo-pizza/">Walt’s Primo
                                                Pizza</a></li>
                                        <li cl
                                            ass="menu-item menu-item-type-post_type menu-item-object-page menu-item-4853">
                                            <a href="https://ballysac.com/restaurants/sack-o-subs/">Sack O’ Subs</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5011">
                                            <a href="https://ballysac.com/restaurants/sportsbook-lounge/">Sportsbook
                                                Lounge</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4962"><a
                                        href="https://ballysac.com/contact/">Contact</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4961"><a
                                        href="https://ballysac.com/careers/">Careers</a></li>
                            </ul>
                        </ul>
                    </aside>
                    <a class="exit-off-canvas"></a></div>
            </header> <!-- end .header -->
            <div class="loader"></div>
            <div class="content">
                @yield('content')
            </div>
        </div> <!-- end #container -->
    </div> <!-- end .inner-wrap -->
</div> <!-- end .off-canvas-wrap -->
<!-- end page -->
<script src="/HeaderFooterAssets/foundation.min.js?ver=5.6"></script>
<script src="/HeaderFooterAssets/scripts.js?ver=5.6"></script>
</div>
</div>
</div>
</div>
</div>
</body>
<footer class="footer" role="contentinfo" style="background: #B02526">
    <div id="inner-footer" class="row clearfix fullwidth">
        <div class="large-12 medium-12 columns">
            <div id="execphp-2" class="widget-odd widget-first widget-1 widget widget_execphp">
                <div class="execphpwidget"><p>An inherent risk of exposure to COVID-19 exists in any public
                        place where people are present. COVID-19 is an extremely contagious disease that can
                        lead to severe illness and even death. According to the <a
                            href="https://www.cdc.gov/coronavirus/2019-nCoV/index.html">Centers for Disease
                            Control and Prevention,</a> senior citizens and guests with underlying medical
                        conditions are especially vulnerable. By visiting Bally's Atlantic City and Casino
                        you voluntarily assume all risks related to exposure to COVID-19.</p>

                    <a href="https://twitter.com/BallysAC"><span class="fa fa-twitter-square"
                                                                 title="Twitter"
                                                                 target="_blank"></span><span
                            class="screen-reader-text">Like Us on Facebook</span></a>

                    <a href="https://www.facebook.com/BallysAtlanticCity"><span
                            class="fa fa-facebook-square" title="Twitter" target="_blank"></span><span
                            class="screen-reader-text">Follow Us on Twitter</span></a>

                    <a href="https://www.instagram.com/ballysac/"><span class="fa fa-instagram"
                                                                        title="instagram"
                                                                        target="_blank"></span><span
                            class="screen-reader-text">Follow Us on Instagram</span></a>


                    <style>a span.screen-reader-text {
                            clip: rect(1px, 1px, 1px, 1px);
                            position: absolute !important;
                            height: 1px;
                            width: 1px;
                            overflow: hidden;
                            color: #fff;
                            font-size: 20px;
                        }</style>
                </div>
            </div>
            <div id="nav_menu-2"
                 class="widget-even widget-2 favorite-links-footer-widget widget widget_nav_menu"><h2
                    class="widgettitle">FAVORITE LINKS</h2>
                <div class="menu-footer-menu-container">
                    <ul id="menu-footer-menu" class="menu">
                        <li id="menu-item-150"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-150">
                            <a href="https://ballysac.com/casino/promotions/">Promotions</a></li>
                        <li id="menu-item-1783"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1783">
                            <a href="https://ballysac.com/hotel/">Hotel</a></li>
                        <li id="menu-item-1784"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1784">
                            <a href="https://ballysac.com/casino/table-games/">Table Games</a></li>
                        <li id="menu-item-174"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-174">
                            <a href="https://ballysac.com/casino/slots-and-video-poker/">Slots and Video
                                Poker</a></li>
                    </ul>
                </div>
            </div>
            <div id="execphp-3"
                 class="widget-odd widget-last widget-3 join-our-mailing-list-widget widget widget_execphp">
                <h2 class="widgettitle">BALLY’S CORP. PROPERTIES</h2>
                <div class="execphpwidget"><a href="https://www.twinriver.com/" target="_blank"
                                              rel="noopener"><img
                            src="/HeaderFooterAssets/Bally_Individual-AllProperties-01.png" width="30%"
                            alt="Twin River Casino Hotel"> </a>
                    <a href="https://www.hrhcbiloxi.com/" target="_blank" rel="noopener"><img
                            src="/HeaderFooterAssets/Bally_Individual-AllProperties-02.png" width="30%"
                            alt="Hard Rock Hotel &amp; Casino Biloxi"> </a>
                    <a href="https://www.goldenmardigras.com/" target="_blank" rel="noopener"><img
                            src="/HeaderFooterAssets/Bally_Individual-AllProperties-03.png" width="30%"
                            alt="Mardi Gras Casinos"> </a>
                    <br>
                    <a href="https://www.eldoradoshreveport.com/" target="_blank" rel="noopener"><img
                            src="/HeaderFooterAssets/Bally_Individual-AllProperties-10.png" width="30%"
                            alt="ElDorado Shreveport"> </a>
                    <a href="https://www.twinrivertiverton.com/" target="_blank" rel="noopener"><img
                            src="/HeaderFooterAssets/Bally_Individual-AllProperties-05.png_" width="30%"
                            alt="Tiverton Casino Hotel"> </a>
                    <a href="https://www.doverdowns.com/" target="_blank" rel="noopener"><img
                            src="/HeaderFooterAssets/Bally_Individual-AllProperties-06.png" width="30%"
                            alt="Dover Downs Hotel &amp; Casino"> </a>
                    <br>
                    <a href="http://www.mihiracing.com/mhre/" target="_blank" rel="noopener"><img
                            src="/HeaderFooterAssets/Bally_Individual-AllProperties-07.png" width="30%"
                            alt="Arapahoe Park"> </a>
                    <a href="https://casinokc.com/" target="_blank" rel="noopener"><img
                            src="/HeaderFooterAssets/Bally_Individual-AllProperties-08.png" width="30%"
                            alt="Casino KC"> </a>
                    <a href="https://casinovb.com/" target="_blank" rel="noopener"><img
                            src="/HeaderFooterAssets/Bally_Individual-AllProperties-09.png" width="30%"
                            alt="Casino Vicksburg"> </a>
                    <br></div>
            </div>
            <div style="clear: both"></div>
            <div id="execphp-13"
                 class="widget-odd widget-last widget-first widget-1 post-footer widget_execphp center">
                <div class="execphpwidget"><p>1900 Pacific Ave.<br>
                        Atlantic City, NJ 08401<br>
                        Tel: (609) 340-2000 </p>

                    <p>Copyright ©2021 Bally's Atlantic City Hotel &amp; Casino®. All rights reserved. | <a
                            href="https://ballysac.com/privacy-policy/">Privacy Policy</a> | <a
                            href="https://ballysac.com/cookie-policy/">Cookie Policy</a></p></div>
            </div>
        </div>
    </div> <!-- end #inner-footer -->
</footer> <!-- end .footer -->
    <script>
        $(window).load(function() {
            $(".loader").fadeOut("slow");
        });
    </script>
</html>


