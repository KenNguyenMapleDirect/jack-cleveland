<!DOCTYPE html>
<html lang="en-US">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <link rel="profile" href="http://gmpg.org/xfn/11">
      <link rel="pingback" href="">
      <title>Jack Cleveland - Jack Entertainment</title>
      <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
      <link rel="canonical" href="https://www.jackentertainment.com/" />
      <meta property="og:locale" content="en_US" />
      <meta property="og:type" content="article" />
      <meta property="og:title" content="Awards and Recognition - Jack Entertainment" />
      <meta property="og:url" content="https://www.jackentertainment.com/" />
      <meta property="og:site_name" content="Jack Entertainment" />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:title" content="Awards and Recognition - Jack Entertainment" />
      <meta name="twitter:site" content="@JACKThistledown" />
      <meta name="twitter:creator" content="@JACKThistledown" />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="alternate" type="application/rss+xml" title="Jack Entertainment &raquo; Feed" href="https://www.jackentertainment.com/feed/" />
      <link rel="alternate" type="application/rss+xml" title="Jack Entertainment &raquo; Comments Feed" href="https://www.jackentertainment.com/comments/feed/" />
      <style type="text/css">
         img.wp-smiley,
         img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
         }
         .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('https://ballysac.maplewebservices.com/loading.gif') 50% 50% no-repeat rgb(255, 255, 255);
        }
      </style>
          <script type="text/javascript" src="{{asset('jackClevelandAssets/js/jquery-1.9.1.js')}}"></script>
          <link rel="dns-prefetch" href="https://maxcdn.bootstrapcdn.com/">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <link rel='stylesheet' id='jackentertainment-bootstrap-css'  href='/jackClevelandAssets/css/bootstrap.min.css?ver=1' type='text/css' media='all' />
      <link rel='stylesheet' id='wp-block-library-css'  href='/jackClevelandAssets/css/style.min.css?ver=5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='contact-form-7-css'  href='/jackClevelandAssets/css//styles.css?ver=5.1.6' type='text/css' media='all' />
      <link rel='stylesheet' id='jack-extension-css'  href='/jackClevelandAssets/css/jack-extension.css?ver=5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='fts-feeds-css'  href='/jackClevelandAssets/css/feedstyles.css?ver=2.9.0' type='text/css' media='' />
      <link rel='stylesheet' id='jackentertainment-style-css'  href='/jackClevelandAssets/css/jackstyle.css?ver=5.3' type='text/css' media='all' />
      <style id='jackentertainment-style-inline-css' type='text/css'>
         #page-content { margin-top: 150px; }
      </style>
      <!-- lightbox css -->
      <link href="/weekender_assets/css/modal_slick.css" rel="stylesheet">
    <link href="/weekender_assets/css/modal_slick-theme.css" rel="stylesheet">
    <link href="/weekender_assets/css/modal_lightbox.min.css" rel="stylesheet">
    <link href="/weekender_assets/css/modal_style.css" rel="stylesheet">
    <script type="text/javascript"
            src="{{asset('flipbook_assets/js/modernizr.2.5.3.min.js')}}"></script>
    <!--end lightbox css -->
      <link rel='stylesheet' id='jackentertainment-font-awesome-css'  href='/jackClevelandAssets/css/font-awesome.min.css?ver=5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='jackentertainment-css-custom-css'  href='/jackClevelandAssets/css/custom.css?ver=5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='jackentertainment-css-styles-css'  href='/jackClevelandAssets/css/styles88.css?ver=0.88' type='text/css' media='all' />
      <link rel='stylesheet' id='owl-carousel-css'  href='/jackClevelandAssets/css/owl.carousel.css?ver=5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='jackentertainment-responsive-css-css'  href='/jackClevelandAssets/css/responsive-grid.css?ver=0.03' type='text/css' media='all' />
      <link rel='stylesheet' id='js_composer_front-css'  href='/jackClevelandAssets/css/js_composer.min.css?ver=6.0.5' type='text/css' media='all' />
      <link rel='stylesheet' id='bsf-Defaults-css'  href='/jackClevelandAssets/css/Defaults.css?ver=5.3' type='text/css' media='all' />
      <script>(function(e){function k(b){try{var a=new e.XMLHttpRequest;a.open("GET",ss_urls.ajaxurl+"?action=ss-get-update-feed&use="+b);a.onload=function(c){"abort"===c.type?a.abort():"error"!==c.type&&200===a.status&&a.responseText&&d.update_stream_feed(b,JSON.parse(a.responseText))};a.send()}catch(c){}setTimeout(function(){k(b)},5E3)}var d={version:"0.0.1"},f={},g={},h="";e.ss_urls={ajaxurl:"https://www.jackentertainment.com/wp-admin/admin-ajax.php",pluginurl:"https://www.jackentertainment.com/wp-content/plugins/cardz-social"};d.init_stream=function(b,a){if(a)d.register_stream(b,a);else try{var c=new e.XMLHttpRequest;
         c.open("GET",ss_urls.ajaxurl+"?action=ss-get-feed-data&use="+b);c.onload=function(a){"abort"===a.type?c.abort():"error"!==a.type&&200===c.status&&c.responseText&&(d.register_stream(b,JSON.parse(c.responseText)),d.trigger_stream(b))};c.send()}catch(f){}};d.register_stream=function(b,a){g[b]=a};d.trigger_stream=function(b){var a="cardz-social-"+b,c=g[b];jQuery("#"+a+" img.loader").remove();f[b]=jQuery("."+a).cardZSocial(c);h=c.feedHash;!0===c.realtime&&k(b)};d.trigger_streams=function(){for(var b in g)d.trigger_stream(b)};
         d.update_stream_feed=function(b,a){f[b]&&a.hash!==h&&(f[b].set_feed_data(a.feed),h=a.hash)};e.ss_client=d})(window);
      </script><script type='text/javascript'>
         /* <![CDATA[ */
         var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[]","home_url":"https:\/\/www.jackentertainment.com","hash_tracking":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.10.0'></script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/plugins/cardz-social/assets/js/script.js?ver=5.3'></script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/plugins/cardz-social/content/js/cardz.social.min.js?ver=5.3'></script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/plugins/feed-them-social/feeds/js/powered-by.js?ver=2.9.0'></script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/plugins/feed-them-social/feeds/js/fts-global.js?ver=2.9.0'></script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/plugins/duracelltomi-google-tag-manager/js/gtm4wp-contact-form-7-tracker.js?ver=1.11.6'></script>
      <link rel='https://api.w.org/' href='https://www.jackentertainment.com/wp-json/' />
      <meta name="generator" content="WordPress 5.3" />
      <link rel='shortlink' href='https://www.jackentertainment.com/?p=9531' />
      <link rel="alternate" type="application/json+oembed" href="https://www.jackentertainment.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.jackentertainment.com%2Fawards%2F" />
      <link rel="alternate" type="text/xml+oembed" href="https://www.jackentertainment.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.jackentertainment.com%2Fawards%2F&#038;format=xml" />
      <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
      <link rel="icon" href="https://www.jackentertainment.com/wp-content/uploads/2016/06/jack-favicon-150x150.png" sizes="32x32" />
      <link rel="icon" href="https://www.jackentertainment.com/wp-content/uploads/2016/06/jack-favicon-300x300.png" sizes="192x192" />
      <link rel="apple-touch-icon-precomposed" href="https://www.jackentertainment.com/wp-content/uploads/2016/06/jack-favicon-300x300.png" />
      <meta name="msapplication-TileImage" content="https://www.jackentertainment.com/wp-content/uploads/2016/06/jack-favicon-300x300.png" />
      <style type="text/css" id="wp-custom-css">
         .jk-page-portal-home .jk-img-cards > div:first-child, 
         #content section:nth-child(3) .jk-img-cards > div:first-child {
         margin-left: 200px
         }
         @media (max-width: 991px) {
         .jk-page-portal-home .jk-img-cards > div:first-child, 
         #content section:nth-child(3) .jk-img-cards > div:first-child {
         margin-left: 0
         }	
         }
         @media (max-width: 991px) {
         .page-id-8752 .jk-page-portal-about section.jk-bg-accent:not(.jk-knowjack) .jk-img-cards:not(.wide) > div:first-child {
         margin-left: 0
         }
         }
         @media (min-width: 992px) {
         .col-md-offset-0 {
         margin-left: 137px;
         }
         .page-id-361 section:nth-child(2) .jk-img-cards > div:first-child {
         margin-left:300px;
         }
         .jk-page-portal-about section.jk-bg-accent:not(.jk-knowjack) .jk-img-cards:not(.wide) > div:first-child {
         margin-left: 200px
         }
         }		
      </style>
      <noscript>
         <style> .wpb_animate_when_almost_visible { opacity: 1; }</style>
      </noscript>
      <script src="https://www.google.com/recaptcha/api.js" async defer></script>
      <!--<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit"></script>-->
      <script type="text/javascript">
         var ajax_base_url = 'https://www.jackentertainment.com/wp-admin/admin-ajax.php';
      </script>
   </head>
   <body class="page-template page-template-page-templates page-template-page-responsive-card-template page-template-page-templatespage-responsive-card-template-php page page-id-9531 group-blog awards wpb-js-composer js-comp-ver-6.0.5 vc_responsive">
   <div id="flipbook-section">
<div id="flipbook-section1">
<div id="flipbook-section2">
<div id="flipbook-section3">
    <div id="weekender-section">   
   <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KQX5688"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->
      <div id="fullscreen">
         <div class="video-wrapper">
         </div>
         <span class="close">X</span>
      </div>
      <header id="masthead" class="site-header " role="banner">
         <div class="header-wrap">
            <div class="container-fluid">
               <div class="jk-header-row row">
                  <div class="jk-header-logo col-xs-12 col-md-2">
                     <a href="https://www.jackentertainment.com/" title="Jack Entertainment"><img class="site-logo" src="http://www.jackentertainment.com/wp-content/uploads/2016/02/jackentertaintment-logo.png" alt="Jack Entertainment" /></a>
                  </div>
                  <div class="xs-hidden col-md-10">
                     <div class="jk-header-area">
                        <div class="jk-menu-container">
                           <nav id="mainnav" class="mainnav" role="navigation">
                              <div class="menu-main-menu-container">
                                 <ul id="menu-main-menu" class="menu">
                                    <li id="menu-item-380" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-380">
                                       <a href="https://www.jackentertainment.com/about-us/">Our Story</a>
                                       <ul class="sub-menu">
                                          <li id="menu-item-9611" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9611"><a href="https://www.jackentertainment.com/about-us/">About Us</a></li>
                                          <li id="menu-item-9610" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-9531 current_page_item menu-item-9610"><a href="https://www.jackentertainment.com/awards/" aria-current="page">Awards</a></li>
                                          <li id="menu-item-9612" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9612"><a href="https://www.jackentertainment.com/community-involvement/">Community Involvement</a></li>
                                       </ul>
                                    </li>
                                    <li id="menu-item-231" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-231">
                                       <a href="#">Locations</a>
                                       <ul class="sub-menu">
                                          <li id="menu-item-680" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-680"><a href="/cleveland/">JACK Cleveland</a></li>
                                          <li id="menu-item-235" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-235"><a href="/thistledown/">JACK Thistledown</a></li>
                                       </ul>
                                    </li>
                                    <li id="menu-item-4355" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4355"><a href="https://www.jackentertainment.com/newsroom/">Newsroom</a></li>
                                    <li id="menu-item-238" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-238"><a href="#contactus">Contact Us</a></li>
                                    <li id="menu-item-2508" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2508"><a href="https://www.jackentertainment.com/careers/">Careers</a></li>
                                    <li class="menu-item menu-item-map-marker"><a href="https://goo.gl/maps/gTqB3N2a3GdqPFMG7" target="_blank"><i class="fa fa-map-marker"></i><span class="desc">Find Us</span></a></li>
                                    <li class="menu-item menu-item-search">
                                       <a class="search-icon" href="#"><i class="fa fa-search"></i></a>
                                       <form method="get" class="menu-search-form" action="https://www.jackentertainment.com">
                                          <p><input class="text_input" type="text" placeholder="Type Something..." name="s" id="s" /><input type="submit" class="my-wp-search jk-btn primary" id="searchsubmit" value="search" /></p>
                                       </form>
                                    </li>
                                 </ul>
                              </div>
                           </nav>                           
                        </div>
                     </div>
                  </div>
               </div>
               <div class="btn-menu"></div>
            </div>
         </div>
      </header>
      </div>
      </div>
      </div>
      </div>
      <!-- Content Container -->
      <div class="container-fluid cards-backdrop text-center" style="padding-top:150px">
      <div class="loader"></div>
            <div class="content">
                @yield('content')
            </div>
      </div>
      <!-- Footer -->
      <div id="sidebar-footer" class="prefooter widget-area jk-bg-dark" role="complementary">
         <div class="container">
            <aside id="jackentertainment_contact_info-2" class="widget jackentertainment_contact_info_widget">
               <h3 id="contactus" class="jk-heading jk-red text-center">Contact</h3>
               <div class="row">
                  <div class="contact-phone col-lg-6 text-center"><span><i class="fa fa-mobile"></i></span><a href="tel:18443095225">844.309.JACK</a></div>
                  <div class="contact-address col-lg-6 text-center"><span><i class="fa fa-map-marker"></i></span><a href='https://goo.gl/maps/6qz8sUy395D31Bqc7' target='blank' id='contact-address'><span style='margin-right: 5px;'>100 Public Square,</span><span style='display: inline-block'>Cleveland, OH 44113</span></a></div>
               </div>
            </aside>
         </div>
      </div>
      <footer id="footer" class="site-footer " role="contentinfo">
         <div class="site-info container">
            <div class="panel-grid">
               <div class="menu-footer-links-container">
                  <ul id="menu-footer-navs" class="menu">
                     <li id="menu-item-2955" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2955"><a href="https://www.jackentertainment.com/clubjack/">ClubJACK</a></li>
                     <li id="menu-item-4356" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4356"><a href="https://www.jackentertainment.com/newsroom/">Media</a></li>
                     <li id="menu-item-1244" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1244"><a href="https://www.jackentertainment.com/club-jack-rules/">ClubJACK Rules</a></li>
                     <li id="menu-item-11519" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11519"><a href="https://www.jackentertainment.com/cleveland/privacy-policy/">Privacy Policy</a></li>
                     <li id="menu-item-1245" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1245"><a href="https://www.jackentertainment.com/responsible-gaming/">Responsible Gaming</a></li>
                     <li id="menu-item-1242" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1242"><a href="https://www.jackentertainment.com/terms-of-use/">Terms of Use</a></li>
                     <li id="menu-item-9613" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-9531 current_page_item menu-item-9613"><a href="https://www.jackentertainment.com/awards/" aria-current="page">Awards</a></li>
                     <li id="menu-item-9614" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9614"><a href="https://www.jackentertainment.com/community-involvement/">Community Involvement</a></li>
                     <li id="menu-item-10213" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10213"><a href="https://www.jackentertainment.com/wp-content/uploads/2020/08/No-Mail-No-Email-Request-Form-v2.pdf">Mailing List Removal Request</a></li>
                     <li id="menu-item-11364" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11364"><a href="https://www.jackentertainment.com/vendors">Vendor Inquiries</a></li>
                  </ul>
               </div>
            </div>
            <div class="footer-caution">
               Must be 21 or older to gamble or participate in promotions. Must be 18 or older for pari-mutuel wagering. Not marketed to and void if used by anyone in the Ohio Voluntary Exclusion Program or anyone banned / excluded by or with JACK Entertainment LLC. <br />
               Gambling Problem? OH: 1-800-589-9966				
               <h4 class="footer-copyright">
                  <a href="/">© 2021 JACK Entertainment LLC</a><br /><small>
                  Trademarks used herein are owned by JACK Entertainment LLC and its affiliated companies.*</small><br />
                  <br />
                  <small><em>*Please see <a href="https://www.jackentertainment.com/terms-of-use/">Terms of Use</a> for more trademark information.</em></small>				
               </h4>
            </div>
         </div>
         <!-- .site-info -->
      </footer>
      <script>
        $(window).load(function() {
            $(".loader").fadeOut("slow");
        });
    </script>
      <!-- #footer -->
      </div>

      <link rel='stylesheet' id='vc_tta_style-css'  href='https://www.jackentertainment.com/wp-content/plugins/js_composer/assets/css/js_composer_tta.min.css?ver=6.0.5' type='text/css' media='all' />
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/themes/jackentertainment/plugins/bootstrap/js/bootstrap.js?ver=5.3'></script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.6'></script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/themes/jackentertainment/js/scripts.js?ver=5.3'></script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/themes/jackentertainment/js/main.js?ver=5.3'></script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/themes/jackentertainment/plugins/owl-carousel/owl.carousel.js?ver=5.3'></script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/themes/jackentertainment/js/skip-link-focus-fix.js?ver=20130115'></script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/themes/jackentertainment/js/jack.js?ver=0.27'></script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/plugins/page-links-to/dist/new-tab.js?ver=3.2.1'></script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-includes/js/wp-embed.min.js?ver=5.3'></script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=6.0.5'></script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/plugins/js_composer/assets/lib/vc_accordion/vc-accordion.min.js?ver=6.0.5'></script>
      <script type='text/javascript' src='https://www.jackentertainment.com/wp-content/plugins/js_composer/assets/lib/vc-tta-autoplay/vc-tta-autoplay.min.js?ver=6.0.5'></script>
      </div>
      </div>
      </div>
      </div>
      </div>
   </body>
</html>