<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <script type="text/javascript"
                        src="{{asset('flipbook_assets/js/modernizr.2.5.3.min.js')}}"></script>
                <link rel="stylesheet" href="{{asset('flipbook_assets/css/lightbox.min.css')}}"/>
                <script type="text/javascript"
                        src="{{asset('flipbook_assets/js/lightbox-second.min.js')}}"></script>
                <style>
                    table {
                        border-collapse: inherit !important;
                    }

                    .lb-details {
                        display: none;
                    }

                    @font-face {
                        font-family: myFirstFont;
                        src: url({{asset('flipbook_assets/ProximaNova-Bold.otf')}}) format('opentype');
                    }

                    .firstname {
                        font-family: myFirstFont;
                        font-weight: 700;
                        font-size: 16pt;
                    }

                    @font-face {
                        font-family: totalfb;
                        src: url({{asset('flipbook_assets/Steelfish-Bold.ttf')}}) format('truetype');

                    }

                    .dollar-amount {
                        font-family: 'totalfb';
                        font-weight: 700;
                        font-size: 16pt;
                    }

                    /* .dollar-amount62 {
                      font-family: 'Steelfish', sans-serif;
                      font-weight: 700;
                      font-size: 16pt;
                    } */

                    #img-magnifier-container-second {
                        display: none;
                        background: rgba(0, 0, 0, 0.8);
                        border: 5px solid rgb(255, 255, 255);
                        border-radius: 20px;
                        box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85),
                        0 0 7px 7px rgba(0, 0, 0, 0.25),
                        inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
                        cursor: none;
                        position: absolute;
                        pointer-events: none;
                        width: 400px;
                        height: 200px;
                        overflow: hidden;
                        z-index: 999;
                    }

                    .glass {
                        position: absolute;
                        background-repeat: no-repeat;
                        background-size: auto;
                        cursor: none;
                        z-index: 1000;
                    }

                    #toggle-zoom {
                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLACK.png')}}");
                        background-size: 40px;
                        display: block;
                        width: 40px;
                        display: none;
                        height: 40px;
                    }

                    #printer {
                        float: right;
                        display: block;
                        width: 40px;
                        height: 40px;
                        margin-right: 20px;
                        display: none;
                    }

                    #toggle-zoom.toggle-on {
                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLUE.png')}}");
                    }

                    @media (hover: none) {
                        .tool-zoom {
                            display: none;
                        }

                        #printer {
                            display: none;
                        }
                    }
                </style>
                <div class="flipbook-viewport-second">
                    <div class="container">
                        <div>
                            <a href={{asset('flipbook_assets/pages/Booklet.pdf')}}"" target="blank"><img
                                    id="printer"
                                    src="{{asset('flipbook_assets/pages/printer-large.png')}}"/></a>
                        </div>
                        <div class="tool-zoom">
                            <a id="toggle-zoom" onclick="toggleZoom()"></a>
                        </div>

                        <div class="arrows">
                            <div class="arrow-prev">
                                <a id="prev-second"><img class="previous" width="20"
                                                         src="{{asset('flipbook_assets/pages/left-arrow.svg')}}"
                                                         alt=""/></a>
                            </div>
                            <center><h4 style="color: red;margin-right: 35px;"> 100K Slot Self
                                    Mailer</h4>
                            </center>
                            <div class="flipbook-second">
                                <!-- Below is where you change the images for the flipbook -->
                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/129396_JUN2021_100K_SLOT_SM_P01.jpg"
                                   data-odd="1"
                                   id="page-1"
                                   data-lightbox="big" class="page"
                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/129396_JUN2021_100K_SLOT_SM_P01.jpg')"></a>

                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/129396_JUN2021_100K_SLOT_SM_P02.jpg"
                                   data-even="2"
                                   id="page-2" data-lightbox="big" class="single"
                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/129396_JUN2021_100K_SLOT_SM_P02.jpg')"></a>

                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/129396_JUN2021_100K_SLOT_SM_P03.jpg"
                                   data-odd="3"
                                   id="page-3"
                                   data-lightbox="big" class="single"
                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/129396_JUN2021_100K_SLOT_SM_P03.jpg')"></a>

                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/lo_res/129396_JUN2021_100K_SLOT_SM_P04.jpg"
                                   data-even="4"
                                   id="page-4" data-lightbox="big" class="single"
                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/129396_JUN2021_100K_SLOT_SM_P04.jpg')"></a>
                            </div>
                            <div class="arrow-next">
                                <a id="next-second"><img class="next-second" width="20"
                                                         src="{{asset('flipbook_assets/pages/right-arrow.svg')}}"
                                                         alt=""/></a>
                            </div>
                        </div>
                    </div>
                </div><!-- Below are the thumbnails to the flipbook -->
                <div class="flipbook-slider-thumb-second">
                    <div class="drag-second" style="margin-right: 35px;">
                        <!-- <img id="prev-arrow" class="thumb-arrow" src="assets/pages/left-arrow.svg" alt=""> -->
                        <img onclick="onPageClickSecond(1)" class="thumb-img left-img"
                             src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/lo_res/129396_JUN2021_100K_SLOT_SM_P01.jpg"
                             alt=""/>

                        <div class="space">
                            <img onclick="onPageClickSecond(2)" class="thumb-img"
                                 src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/lo_res/129396_JUN2021_100K_SLOT_SM_P02.jpg"
                                 alt=""/>
                            <img onclick="onPageClickSecond(3)" class="thumb-img"
                                 src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/lo_res/129396_JUN2021_100K_SLOT_SM_P03.jpg"
                                 alt=""/>
                        </div>

                        <div class="space">
                            <img onclick="onPageClickSecond(4)" class="thumb-img"
                                 src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/lo_res/129396_JUN2021_100K_SLOT_SM_P04.jpg"
                                 alt=""/>
                        </div>
                    </div>

                    <ul class="flipbook-slick-dots-second" role="tablist">
                        <li onclick="onPageClickSecond(1)" class="dot">
                            <a type="button" style="color: #7f7f7f">1</a>
                        </li>
                        <li onclick="onPageClickSecond(2)" class="dot">
                            <a type="button" style="color: #7f7f7f">2</a>
                        </li>
                        <li onclick="onPageClickSecond(3)" class="dot">
                            <a type="button" style="color: #7f7f7f">3</a>
                        </li>
                        <li onclick="onPageClickSecond(4)" class="dot">
                            <a type="button" style="color: #7f7f7f">4</a>
                        </li>
                    </ul>
                </div>
                <div id="img-magnifier-container">
                    <img id="zoomed-image-container" class="glass" src=""/>
                </div>
                <div id="log"></div>
                <audio id="audio" style="display: none"
                       src="{{asset('flipbook_assets/page-flip.mp3')}}"></audio>
                <script type="text/javascript">
                    function scaleFlipBookSecond() {
                        var imageWidth = 541;
                        var imageHeight = 850;

                        var pageHeight = 275;
                        var pageWidth = parseInt((pageHeight / imageHeight) * imageWidth);

                        $(".flipbook-viewport-second .container").css({
                            width: 40 + pageWidth * 2 + 40 + "px",
                        });

                        $(".flipbook-viewport-second .flipbook-second").css({
                            width: pageWidth * 2 + "px",
                            height: pageHeight + "px",
                        });
                    }

                    function doResizeSecond() {
                        $("html").css({
                            zoom: 1
                        });
                        var $viewportSecond = $(".flipbook-viewport-second");
                        var viewHeight = $viewportSecond.height();
                        var viewWidth = $viewportSecond.width();

                        var $el = $(".flipbook-viewport-second .container");
                        var elHeight = $el.outerHeight();
                        var elWidth = $el.outerWidth();

                        var scale = Math.min(viewWidth / elWidth, viewHeight / elHeight);
                        //console.log(viewWidth , elWidth, viewHeight , elHeight, scale);
                        if (scale < 1) {
                            scale *= 0.95;
                        } else {
                            scale = 1;
                        }
                        // $("html").css({
                        //     zoom: scale
                        // });
                    }

                    function loadAppSecond() {
                        scaleFlipBookSecond();
                        var flipbookSecond = $(".flipbook-second");

                        // Check if the CSS was already loaded

                        if (flipbookSecond.width() == 0 || flipbookSecond.height() == 0) {
                            setTimeout(loadAppSecond, 10);
                            return;
                        }

                        $(".flipbook-second .double").scissor();

                        // Create the flipbook-second

                        $(".flipbook-second").turn({
                            // Elevation

                            elevation: 50,

                            // Enable gradients

                            gradients: true,

                            // Auto center this flipbook-second

                            autoCenter: true,
                            when: {
                                turning: function (event, page, view) {
                                    var audio = document.getElementById("audio");
                                    audio.play();
                                },
                                turned: function (e, page) {
                                    //console.log('Current view: ', $(this).turn('view'));
                                    var thumbs = document.getElementsByClassName("thumb-img");
                                    for (var i = 0; i < thumbs.length; i++) {
                                        var element = thumbs[i];
                                        if (element.className.indexOf("active") !== -1) {
                                            $(element).removeClass("active");
                                        }
                                    }

                                    $(
                                        document.getElementsByClassName("thumb-img")[page - 1]
                                    ).addClass("active");

                                    var dots = document.getElementsByClassName("dot");
                                    for (var i = 0; i < dots.length; i++) {
                                        var dot = dots[i];
                                        if (dot.className.indexOf("dot-active") !== -1) {
                                            $(dot).removeClass("dot-active");
                                        }
                                    }
                                },
                            },
                        });
                        doResizeSecond();
                    }

                    $(window).resize(function () {
                        doResizeSecond();
                    });
                    $(window).bind("keydown", function (e) {
                        if (e.keyCode == 37) $(".flipbook-second").turn("previous");
                        else if (e.keyCode == 39) $(".flipbook-second").turn("next");
                    });

                    $("#prev-second").click(function (e) {
                        e.preventDefault();
                        $(".flipbook-second").turn("previous");
                    });

                    $("#next-second").click(function (e) {
                        e.preventDefault();
                        $(".flipbook-second").turn("next");
                    });

                    $("#prev-arrow-second").click(function (e) {
                        e.preventDefault();
                        $(".flipbook-second").turn("previous");
                    });

                    $("#next-arrow-second").click(function (e) {
                        e.preventDefault();
                        $(".flipbook-second").turn("next");
                    });

                    function onPageClickSecond(i) {
                        $(".flipbook-second").turn("page", i);
                    }

                    // Load the HTML4 version if there's not CSS transform
                    yepnope({
                        test: Modernizr.csstransforms,
                        yep: ["{{asset('flipbook_assets/js/turn.min.js')}}"],
                        nope: ["{{asset('flipbook_assets/js/turn.html4.min.js')}}"],
                        both: ["{{asset('flipbook_assets/js/scissor.min.js')}}", "{{asset('flipbook_assets/css/double-page-second.css')}}"],
                        complete: loadAppSecond,
                    });

                    zoomToolEnabled = false;

                    function toggleZoom() {
                        if (zoomToolEnabled) {
                            $(".flipbook-second a").off("mousemove");
                            $("#toggle-zoom-second").removeClass("toggle-on");
                            $("#img-magnifier-container-second").hide();

                            zoomToolEnabled = false;
                        } else {
                            $(".flipbook-second a").mousemove(function (event) {
                                var magnifier = $("#img-magnifier-container-second");
                                $("#img-magnifier-container-second").css(
                                    "left",
                                    event.pageX - magnifier.width() / 2
                                );
                                $("#img-magnifier-container-second").css(
                                    "top",
                                    event.pageY - magnifier.height() / 2
                                );
                                $("#img-magnifier-container-second").show();
                                var hoveredImage = $(event.target).css("background-image");
                                var bg = hoveredImage
                                    .replace("url(", "")
                                    .replace(")", "")
                                    .replace(/\"/gi, "");
                                // Find relative position of cursor in image.
                                var targetPage = $(event.target);
                                var targetLeft = 400 / 2; // Width of glass container/2
                                var targetTop = 200 / 2; // Height of glass container/2

                                var zoomedImageContainer = document.getElementById(
                                    "zoomed-image-container"
                                );
                                var zoomedImageWidth = zoomedImageContainer.width;
                                var zoomedImageHeight = zoomedImageContainer.height;

                                var imgXPercent =
                                    (event.pageX - $(event.target).offset().left) /
                                    targetPage.width();
                                targetLeft -= zoomedImageWidth * imgXPercent;
                                var imgYPercent =
                                    (event.pageY - $(event.target).offset().top) /
                                    targetPage.height();
                                targetTop -= zoomedImageHeight * imgYPercent;

                                $("#img-magnifier-container-second .glass").attr("src", bg);
                                $("#img-magnifier-container-second .glass").css(
                                    "top",
                                    "" + targetTop + "px"
                                );
                                $("#img-magnifier-container-second .glass").css(
                                    "left",
                                    "" + targetLeft + "px"
                                );
                            });

                            $("#toggle-zoom-second").addClass("toggle-on");
                            zoomToolEnabled = true;
                        }
                    }
                </script>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <script type="text/javascript"
                        src="{{asset('flipbook_assets/js/modernizr.2.5.3.min.js')}}"></script>
                <link rel="stylesheet" href="{{asset('flipbook_assets/css/lightbox.min.css')}}"/>
                <script type="text/javascript"
                        src="{{asset('flipbook_assets/js/lightbox.min.js')}}"></script>
                <script type="text/javascript"
                        src="{{asset('flipbook_assets/js/dynamic-content.js')}}"></script>
                <style>
                    table {
                        border-collapse: inherit !important;
                    }

                    .lb-details {
                        display: none;
                    }

                    @font-face {
                        font-family: myFirstFont;
                        src: url({{asset('flipbook_assets/ProximaNova-Bold.otf')}}) format('opentype');
                    }

                    .firstname {
                        font-family: myFirstFont;
                        font-weight: 700;
                        font-size: 16pt;
                    }

                    @font-face {
                        font-family: totalfb;
                        src: url({{asset('flipbook_assets/Steelfish-Bold.ttf')}}) format('truetype');

                    }

                    .dollar-amount {
                        font-family: 'totalfb';
                        font-weight: 700;
                        font-size: 16pt;
                    }

                    /* .dollar-amount62 {
                      font-family: 'Steelfish', sans-serif;
                      font-weight: 700;
                      font-size: 16pt;
                    } */

                    #img-magnifier-container {
                        display: none;
                        background: rgba(0, 0, 0, 0.8);
                        border: 5px solid rgb(255, 255, 255);
                        border-radius: 20px;
                        box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85),
                        0 0 7px 7px rgba(0, 0, 0, 0.25),
                        inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
                        cursor: none;
                        position: absolute;
                        pointer-events: none;
                        width: 400px;
                        height: 200px;
                        overflow: hidden;
                        z-index: 999;
                    }

                    .glass {
                        position: absolute;
                        background-repeat: no-repeat;
                        background-size: auto;
                        cursor: none;
                        z-index: 1000;
                    }

                    #toggle-zoom {
                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLACK.png')}}");
                        background-size: 40px;
                        display: block;
                        width: 40px;
                        display: none;
                        height: 40px;
                    }

                    #printer {
                        float: right;
                        display: block;
                        width: 40px;
                        height: 40px;
                        margin-right: 20px;
                        display: none;
                    }

                    #toggle-zoom.toggle-on {
                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLUE.png')}}");
                    }

                    @media (hover: none) {
                        .tool-zoom {
                            display: none;
                        }

                        #printer {
                            display: none;
                        }
                    }
                </style>
                <div class="flipbook-viewport">
                    <div class="container">
                        <div>
                            <a href={{asset('flipbook_assets/pages/Booklet.pdf')}}"" target="blank"><img
                                    id="printer"
                                    src="{{asset('flipbook_assets/pages/printer-large.png')}}"/></a>
                        </div>
                        <div class="tool-zoom">
                            <a id="toggle-zoom" onclick="toggleZoom()"></a>
                        </div>

                        <div class="arrows">
                            <div class="arrow-prev">
                                <a id="prev"><img class="previous" width="20"
                                                  src="{{asset('flipbook_assets/pages/left-arrow.svg')}}"
                                                  alt=""/></a>
                            </div>
                            <center><h4 style="color: red;margin-right: 35px;"> June Core Self
                                    Mailer</h4>
                            </center>
                            <div class="flipbook">
                                <!-- Below is where you change the images for the flipbook -->
                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->juneCoreSMResult1}}"
                                   data-odd="1"
                                   id="page-1"
                                   data-lightbox="big" class="page"
                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->juneCoreSMResult1}}')"></a>

                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->juneCoreSMResult2}}"
                                   data-even="2"
                                   id="page-2" data-lightbox="big" class="single"
                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->juneCoreSMResult2}}')"></a>

                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->juneCoreSMResult3}}"
                                   data-odd="3"
                                   id="page-3"
                                   data-lightbox="big" class="single"
                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->juneCoreSMResult3}}')"></a>

                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->juneCoreSMResult4}}"
                                   data-even="4"
                                   id="page-4" data-lightbox="big" class="single"
                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->juneCoreSMResult4}}')"></a>

                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->juneCoreSMResult5}}"
                                   data-odd="5"
                                   id="page-5"
                                   data-lightbox="big" class="single"
                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->juneCoreSMResult5}}')"></a>

                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/lo_res/{{$data->juneCoreSMResult6}}"
                                   data-even="6"
                                   id="page-6" data-lightbox="big" class="single"
                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/lo_res/{{$data->juneCoreSMResult6}}')"></a>
                            </div>
                            <div class="arrow-next">
                                <a id="next"><img class="next" width="20"
                                                  src="{{asset('flipbook_assets/pages/right-arrow.svg')}}"
                                                  alt=""/></a>
                            </div>
                        </div>
                    </div>
                </div><!-- Below are the thumbnails to the flipbook -->
                <div class="flipbook-slider-thumb">
                    <div class="drag" style="margin-right: 35px;">
                        <!-- <img id="prev-arrow" class="thumb-arrow" src="assets/pages/left-arrow.svg" alt=""> -->
                        <img onclick="onPageClick(1)" class="thumb-img left-img"
                             src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->juneCoreSMResult1}}"
                             alt=""/>

                        <div class="space">
                            <img onclick="onPageClick(2)" class="thumb-img"
                                 src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->juneCoreSMResult2}}"
                                 alt=""/>
                            <img onclick="onPageClick(3)" class="thumb-img"
                                 src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->juneCoreSMResult3}}"
                                 alt=""/>
                        </div>

                        <div class="space">
                            <img onclick="onPageClick(4)" class="thumb-img"
                                 src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->juneCoreSMResult4}}"
                                 alt=""/>
                            <img onclick="onPageClick(5)" class="thumb-img"
                                 src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->juneCoreSMResult5}}"
                                 alt=""/>
                        </div>

                        <div class="space">
                            <img onclick="onPageClick(6)" class="thumb-img active"
                                 src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/lo_res/{{$data->juneCoreSMResult6}}"
                                 alt=""/>
                        </div>

                    <!-- <img id="next-arrow" class="thumb-arrow" src="{{asset('flipbook_assets/pages/right-arrow.svg')}}" alt=""> -->
                    </div>

                    <ul class="flipbook-slick-dots" role="tablist">
                        <li onclick="onPageClick(1)" class="dot">
                            <a type="button" style="color: #7f7f7f">1</a>
                        </li>
                        <li onclick="onPageClick(2)" class="dot">
                            <a type="button" style="color: #7f7f7f">2</a>
                        </li>
                        <li onclick="onPageClick(3)" class="dot">
                            <a type="button" style="color: #7f7f7f">3</a>
                        </li>
                        <li onclick="onPageClick(4)" class="dot">
                            <a type="button" style="color: #7f7f7f">4</a>
                        </li>
                        <li onclick="onPageClick(5)" class="dot">
                            <a type="button" style="color: #7f7f7f">5</a>
                        </li>
                        <li onclick="onPageClick(6)" class="dot">
                            <a type="button" style="color: #7f7f7f">6</a>
                        </li>
                    </ul>
                </div>
                <div id="img-magnifier-container">
                    <img id="zoomed-image-container" class="glass" src=""/>
                </div>
                <div id="log"></div>
                <audio id="audio" style="display: none"
                       src="{{asset('flipbook_assets/page-flip.mp3')}}"></audio>
                <script type="text/javascript">
                    function scaleFlipBook() {
                        var imageWidth = 541;
                        var imageHeight = 850;

                        var pageHeight = 275;
                        var pageWidth = parseInt((pageHeight / imageHeight) * imageWidth);

                        $(".flipbook-viewport .container").css({
                            width: 40 + pageWidth * 2 + 40 + "px",
                        });

                        $(".flipbook-viewport .flipbook").css({
                            width: pageWidth * 2 + "px",
                            height: pageHeight + "px",
                        });
                    }

                    function doResize() {
                        $("html").css({
                            zoom: 1
                        });
                        var $viewport = $(".flipbook-viewport");
                        var viewHeight = $viewport.height();
                        var viewWidth = $viewport.width();

                        var $el = $(".flipbook-viewport .container");
                        var elHeight = $el.outerHeight();
                        var elWidth = $el.outerWidth();

                        var scale = Math.min(viewWidth / elWidth, viewHeight / elHeight);
                        //console.log(viewWidth , elWidth, viewHeight , elHeight, scale);
                        if (scale < 1) {
                            scale *= 0.95;
                        } else {
                            scale = 1;
                        }
                        // $("html").css({
                        //     zoom: scale
                        // });
                    }

                    function loadApp() {
                        scaleFlipBook();
                        var flipbook = $(".flipbook");

                        // Check if the CSS was already loaded

                        if (flipbook.width() == 0 || flipbook.height() == 0) {
                            setTimeout(loadApp, 10);
                            return;
                        }

                        $(".flipbook .double").scissor();

                        // Create the flipbook

                        $(".flipbook").turn({
                            // Elevation

                            elevation: 50,

                            // Enable gradients

                            gradients: true,

                            // Auto center this flipbook

                            autoCenter: true,
                            when: {
                                turning: function (event, page, view) {
                                    var audio = document.getElementById("audio");
                                    audio.play();
                                },
                                turned: function (e, page) {
                                    //console.log('Current view: ', $(this).turn('view'));
                                    var thumbs = document.getElementsByClassName("thumb-img");
                                    for (var i = 0; i < thumbs.length; i++) {
                                        var element = thumbs[i];
                                        if (element.className.indexOf("active") !== -1) {
                                            $(element).removeClass("active");
                                        }
                                    }

                                    $(
                                        document.getElementsByClassName("thumb-img")[page - 1]
                                    ).addClass("active");

                                    var dots = document.getElementsByClassName("dot");
                                    for (var i = 0; i < dots.length; i++) {
                                        var dot = dots[i];
                                        if (dot.className.indexOf("dot-active") !== -1) {
                                            $(dot).removeClass("dot-active");
                                        }
                                    }
                                },
                            },
                        });
                        doResize();
                    }

                    $(window).resize(function () {
                        doResize();
                    });
                    $(window).bind("keydown", function (e) {
                        if (e.keyCode == 37) $(".flipbook").turn("previous");
                        else if (e.keyCode == 39) $(".flipbook").turn("next");
                    });
                    $("#prev").click(function (e) {
                        e.preventDefault();
                        $(".flipbook").turn("previous");
                    });

                    $("#next").click(function (e) {
                        e.preventDefault();
                        $(".flipbook").turn("next");
                    });

                    $("#prev-arrow").click(function (e) {
                        e.preventDefault();
                        $(".flipbook").turn("previous");
                    });

                    $("#next-arrow").click(function (e) {
                        e.preventDefault();
                        $(".flipbook").turn("next");
                    });

                    function onPageClick(i) {
                        $(".flipbook").turn("page", i);
                    }

                    // Load the HTML4 version if there's not CSS transform
                    yepnope({
                        test: Modernizr.csstransforms,
                        yep: ["{{asset('flipbook_assets/js/turn.min.js')}}"],
                        nope: ["{{asset('flipbook_assets/js/turn.html4.min.js')}}"],
                        both: ["{{asset('flipbook_assets/js/scissor.min.js')}}", "{{asset('flipbook_assets/css/double-page.css')}}"],
                        complete: loadApp,
                    });

                    zoomToolEnabled = false;

                    function toggleZoom() {
                        if (zoomToolEnabled) {
                            $(".flipbook a").off("mousemove");
                            $("#toggle-zoom").removeClass("toggle-on");
                            $("#img-magnifier-container").hide();

                            zoomToolEnabled = false;
                        } else {
                            $(".flipbook a").mousemove(function (event) {
                                var magnifier = $("#img-magnifier-container");
                                $("#img-magnifier-container").css(
                                    "left",
                                    event.pageX - magnifier.width() / 2
                                );
                                $("#img-magnifier-container").css(
                                    "top",
                                    event.pageY - magnifier.height() / 2
                                );
                                $("#img-magnifier-container").show();
                                var hoveredImage = $(event.target).css("background-image");
                                var bg = hoveredImage
                                    .replace("url(", "")
                                    .replace(")", "")
                                    .replace(/\"/gi, "");
                                // Find relative position of cursor in image.
                                var targetPage = $(event.target);
                                var targetLeft = 400 / 2; // Width of glass container/2
                                var targetTop = 200 / 2; // Height of glass container/2

                                var zoomedImageContainer = document.getElementById(
                                    "zoomed-image-container"
                                );
                                var zoomedImageWidth = zoomedImageContainer.width;
                                var zoomedImageHeight = zoomedImageContainer.height;

                                var imgXPercent =
                                    (event.pageX - $(event.target).offset().left) /
                                    targetPage.width();
                                targetLeft -= zoomedImageWidth * imgXPercent;
                                var imgYPercent =
                                    (event.pageY - $(event.target).offset().top) /
                                    targetPage.height();
                                targetTop -= zoomedImageHeight * imgYPercent;

                                $("#img-magnifier-container .glass").attr("src", bg);
                                $("#img-magnifier-container .glass").css(
                                    "top",
                                    "" + targetTop + "px"
                                );
                                $("#img-magnifier-container .glass").css(
                                    "left",
                                    "" + targetLeft + "px"
                                );
                            });

                            $("#toggle-zoom").addClass("toggle-on");
                            zoomToolEnabled = true;
                        }
                    }
                </script>
            </div>
        </div>
    </div>
</div>
