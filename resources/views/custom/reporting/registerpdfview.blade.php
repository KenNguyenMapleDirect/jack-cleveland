<html>
<head>
<style>
    body{
        text-align: center;
    }
    #logo{

    }
    #tableData{
        margin:0 auto;
    }
    #total-signup{
        padding-top:20px;
    }
    .report-title {
        font-size: 20px;
        color: #000000;
        font-weight: 700;
    }
    .trTable{
        text-align: center;
    }
    th{
        font-weight: normal;
    }
    table, th, td {
        border: 1px solid black;
        /*border-collapse: collapse;*/
    }
</style>
</head>
<body>
<img alt="" id="logo"
     src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK8AAAA9CAYAAADbPDk9AAARvUlEQVR4nO1dB5RVxRn+2Bq6IB0roqsiQSAaVzF2EQELGERDAhixIcFo1GBirAkmIGqiElEkKhCCRKMIGIMgdhCCNBV0V40iqysgZSlbPbP5hjOOM3Pnlvdk4X3n7Nn37p039965/8zf/6l3yIEHIYMMIqIhgAMBVAE4BMAPARQAuBDAYwCGpHJgczJvLYMYyAKwAEALQxcdUj2wWZk3l0EMbAFwi+Xn76R6YDMrbwZx8SCAMwGcq/XznqPfbACduTrXB1CPx7cD+AzACgBbg+5LEu/3AfwYwPsAyj1XZNGmGsBmAJ8DWM3PSUPIVIMBfApgm3Zv4vP3AMzmQ8fBPgCuArCBf/rELqdcVwrg0RjXOQZAbwBrANQoL05CyI9HAJgH4GVHPycC6MMXXUmCEH+bALzJdxIVeQBG8F7WAsjV+hH33RrAPwF8wvFXiVeM3xPabzqxzdkk3CaOe9vO9/06gBcB/AdAid5IviCxxBcC+G2MBy6n/PMQHyopfAygI4DbHP2NAnBXzOt9BaAdgN8HtDs/5nXeBfAIFwwb/gdgQkA/azgJbrCc/xWAuyPeYzlXxDscbb4A8DA/52vnfgZgPT8fDWA8gONCXF9c+1D+iYVrEoBL9EZyFavkLP4WdYeAmK1nAJgB4COuMEnhUgDrHH2dldB1rgbwtOP8bwD8K+Y1tpLNbnO06e3BScTKehSAaZbzYw2sPAzupEhgg+BSZTzXV2nTD8Asfr4VwNKQhGuCzp1qobLgHQCejHkRCcHqFwEYnlB/5bw/G07iDE8Ckx19TE3oGp87iE6wyZUh+roIwHzLuVER7k3FU5bjUxTu2pTi42iazuTkn+FQ5lRU8c+FGtM5Xa5729KBkIUXcqYJWaQYQCMAPwAwgA9gwv0AigA87/EQLhwG4OCANhc77j8M3iQn0sdmFTlKUlhm6ccl59ogRIe3DOeE3XUo2W4UtDP8Zoc2KbaSU6gE+EcA/R3XmwlgIuX1bSTO+hSl+lL/stHULuiKWamlnZADfwrgCrKTqZTJLqOi87DldwIPUJGIg77ab6sMs3FgQtaTEov49HECfauwra5RRLfFJBgT7uCKGAX9DL+5kUqahL5y9nDI4cup9J4D4BkugiXkRGJheBbAMADNKZY4LQ468drksCDrgyDiMZZzwhzyk4DfB+E85bywLd5reLD9qcnGRbVl0FwyahTYxKDKiP3dYbEwtCfBhUWhNu4C/wXw54B+LrYc/5CcoNjjPqqp5LWk4re/qZGvk8IoMGu4gWzAhFM9r2NCF85miQ+oSZvsiLaBSwJJO3QqEu6vjAqlCTd5iF06TJaGWz1+Z1OexwXoLSbs4Pv/i+lk0i/EpuwcEaPPM7TvL/C/SY6+wDZLd0PkpeCWhBz5iuG4ENuuDdGPWARO0449QVnVhQLHJJkX4voq1lKc+BaSJt5XLcejylwwrKZSS3/R0Fa8pJNjXCudiKsH2GCzd19NR0EQ6hvk5zJP0WNfy/EtmpycCJImXpuA/VXE/oS40FX5/ppiUVhgER0uinitdMNo/kkAwts1x9LNaI/uhe6yn3bs1wF2dgndEyfROBWBOkkTr8m0AmrDUaCbW/6tfX/G0GcvarR7M2yyb1+atWwoNNjmV9Hk6QPXInVm0u8jaeLVtVOJ5yL0lUt7nwpd5rI5DQZGuN6eBOHVusfyPC73t8mdHEZWXsu4BBOuc4gVkZAk8QoP1+WG44KFzY3QX3+aeSReNzghllvk7FRaHeoKRCzIl4Z77WLxfA7myqvicUVB9sGXFl0EDOR5iYFUiSAp4i2grKVjPW3AUTBA+80USx+mWIMjAZyS0LPVVWyiecqEazSFsbFBHt5KWTcsbPI2GItRFBCU5A1f4i23HG/NABChOLXVzn3J4JxPI9zXvpqMtNMRMGOLYBsU4bp7Gu6iXVxHR00uvtvw/m70VNJ0POiw94N60TLK0bE8or7EeyrtrUP5UOPIrksoy6io4cxuSa9KFPTSzGtzHAP5kcUOeC7jL/Zm1DiCY6Tpq4AuWRWrAiLKgnCpR5vhXN1NoqYXfIl3KGWfRzmbfwngBEvbzYz1jDOrdHeyLQJLQrdCgKu3LnrsjZhqceg0oKvX5DUbGXOcRERhT492Ig74rxRxwiiGtUhFDltTDlgFvTJhV79DNRfjuoAYW9AKYYoJ2NvlXgnb6jvCYJl5zKF0hcELXOB83OBNKLqUhLEUpToBcxBX4jCmqwu173McMrfEJ5ZY5AEO2/PehEWGtBwT1kdZAR14nRzQN7NG6FB/pyh4elBjX9a+gHJlNl2FmyhPiRnTnSFurSy/rccbyuesDsIF2nnpq9/P4pXKIvG+ZPCu5dFWfJ/nc+7JuI2LiSvIahTzz5LEFr7TU2gx0hVDEw5k3tp0mj2Nweq+xDsxYOYOo+fmHrJ9E/5GM4kt/gFkM12U75UMf5zo4U7dwVA6nZv0zxBvLYoYs2Azfy0PiMuOi/nkgkNpoWru0d8AZskUmpR/X7HBh8hnMePBlQg5MaCPwYbrNuV9Zgf8NbQ8j8iy7eZx/3sDbmFypwkj0vT8kyhKXOhpRm3NZNOu+okk43klRjl84Yc5TCMNYiYMupDxuP0f5RbOt5ZBT+nEdIavDvOI882hl/YbIkeqFLYRjnwyW9TXaQ65OS4Gpih+ti4iVdFsUfEIuaYtjUmiuV4OIJUVc+6zJP71YMzCWu24ni/1Gg3lrTxX/hqmwVxmiOltz2Nh/PR7KsJw0XShmrL4XL4j2z32oeu/tpRUKon3ebIpfcXL5vKvEm8zQzGPcY7UaxdKLQHp/TLEu9tjLgO8FjoCeAZIx0oq7bwl1HBN0NOa+2nHNsdIG5nLVH0d/WNmdGSQHiynRcKGXdkgqXZS7PRspzsxXo6RfQGLR66FwYacwe6JaZaAIpBL1yLVxNvAclytD9HJ4E2JW7nHFqSesTrUHdhiwKvlh1QSb1tLJulXWuUZXdbdEjHzQsUySwWZMx1OlAx2L9iKvOwqypJK4j3bkpA3WyuFqkeQzUrIRTnDcvy7TNDMVoLAU6n1Z30HVoU8RwJmFNg8cEvkh1QSry2sTmXpQmQ4XDsfVBvAF7ZqjnrgT7qQy5JZUmYLKi4XFd0Zz5Fue+5JnoX1fGErXrIrUyNVxHs/CwjrWKCUv4RBgdqQIPGusYT2Hcl0lHRjISeOzCvzVWbD4EiOcWCROg01CRD7ImZnmNLBwqK3hX7m8b3WQideGzEHhSSqmOoobaof11n4HMq8ScFWndJlioGF5UZdKYcwplX45n+nHE/S49eERb1XMf7ZVZi62nAsCTFjE0Mfe/Gznvnti6MdIt83ip/rTorGlh8NpKIlZNWN/LyBq4ewKBxAxWukI715CAdX4hymoKhIYtaqeM5SAHAQCanMcC7bEkDfnStLGYNbSvl5K/82K2GiHcj2fq744xdrSqTNCH8R38NOXmcjr1PGa5RRBGnKe+pPDiarkweVkzVdtxHfY9yFYzX/N2HswmrWsJvpubJfxaqiJvwBwBvqcZ14bbEFvQ3FKjZxgJsayrrrGGmI5dXLB+1MKIJfxXt0M+spS60YdG0qJteKkUw6OrC8qwlVShVJW3SbLg7ZRJfTHYHY5ayLIFbt+pY2QUHnplpugtiOTWD8X2RRP4kCFoYpp3dzNq0IRZz8jdnmNHLDlpZ+J5sKqejEG6YUv49cVcVIsVnacRFJdLx27LOYm4DYMNOSb3cLRRzdC1gYwW2e7eBaEnqe3bdC/DyQFyBuLKXsacN+jnd8dgLE+75lk5g8xiX0UY5VeYorT1HRBdvuS05Rqq4QTRPYLESimsl9zQyE241JdzoOZkp20mhm6S+b96avYKlImV9MhU2ibYqSQ6cHnB/k0GuuTUCR/cRR8V1HtgfhXqmV/GpE8VNELfaUD7IPl3Xbi/ZBKWfulexvpEGGuo52OtsAzk94I5ZxAdUNC8jGpGY7IcEJrEItmNKSWrPN+xgHrkCmYR6F9pYaquaEhWlhCounOU56X+1I8EJkbSTZYyfKhhO4agbNiHpst53KyjseFa/zOXNuo99aZX81ZCOdeS8m71hYtGEa0fX0yujsVl6zK1f9Ioot11BJSiLiTu4TpyYgHkVF8k/8noQzoSHFrjWONnmMxiqyiB4VTMFqE/NeHmLGTC+Khp0oT5v0CIltzFV8lfKtbe+PLWwr/qoyG2dnkA7kUN5uwQUsh5O2khnLRRbLj45cWraEyLE9Q7wZ1FlkNs7OoM4iQ7wZ1FlkiDeDOgsT8WanINM2yxAul69o2nkRtfvcCBuTZHt4BGXfeYbPYa+ljrEaNpgTss+sNCw2Pk6DfOU+or43dVzyA/rIVsbsG3RkGowqluZZTRNYr4AbuYtmpyXMP5pJz9VC7vK4hsmWepTQ8bR3vsmM3yib51Vwj64PWcXbJ0etirEAr7FElK3aZQXt0u8aNs5rpjzXStbkmkwT2HKm/W/hMTUQpprVYpZwjFxVY/IZ57CE41vBMZIRYHpp2SjowPErY5+Vyu6i77Hsko7ODKB6i/vhqe+tkRIctEIZl5kcl2WMz/iHMi7NeL7YYmOXXtoPOBa73Ns2iu/DAiFgCSdXtev2fBHvcDL8giaRx2j0PpiOh43a7xbzeEPNHx4Gbbk9vujjID7g7R6/f4E7LDambbGnJbN4IQNCJmmRdYUs4zqfzpkTGFT/BcNBd3IMV2v9VfLldaM3yrZV63F0+DRgmzs5QbM43jcz8Me0h4QvhC32EmU8niURN6ZddhArmOsZDYt5D50UW7U6LhvoZPhCGZeNXADKGCOjjksJz82mk2UwFyIVfbhJznjVn2AylR1NoutFg/oYGvrHGgYlhwOqvtjFjHY6xmMXoGISd+eQO51LzOBgFJLQ8mgH9Nnzax1ZUDW9OYMMWwd05opxvhbc3oDPLFednozmWsLNxF0YyfprYyx79HZXxu0KGv1NyFVW4rCYQI9bMa9nSnbNpQhhCod9hfU3emiVdmzjstISn6viWMWFfqqyk72MNLtdD3Y3iQ3T+PKfJ0up4kCbopEqDQ8n5Tgf+Sxb+x8GhfR7X002LWNlfVfxNowF6EgCmEw26INtGruUHMzHW6bKiyZIF+59DsIVqIhIuF2VSujnObK0Kxxx3PK6pr2ro47LIiW+ex7H53wS7mhTloapomKBEn62U4mH9WHH6cTdHCxZvG88v1/B1cQH7cnGO9JfPsaxCUk60EoJh0wqo0SHTK9ZEbB3xHeBaeTyoLjyFEVS44KkE6+MVx1FZaRYmQ1DfAr+pglnceXdQQVhPZUDuQeYq1KlCcVUXt7nlgUy9d7HZZkkapSVK1V7KMtK5S130/ptY6kvtaEsrifo7oJKvMOZDHkxsxwu4bFjFcq/OT33H4gpzDLoppSmH055821OsrChjRuYA7aQ1ohJjoDvJGBi+aVKJsRVKbqujCtuoyhsuxtkYI6t8EgtJPE2oJb8BquYf8CHnEOtcTRXpR955CZJOcmUK6VD5oWZzGQHkJh03EAT0/VkLfNYRXsBH1oqlmM9NqzTOU8lNf0nyWmkBSKoBKd8Dh8ZVLa17dVwE9scw+vbQidba4W4fbFCMf2NVwK9TehCK44OKcMGveMw4xIa8uXJgHGX7VAqRNMdWQA5yh4QQTuM76OkHd3LipAPUO5cwAmkOzZOZCnMFY7A6ynUgFuzGrsJkiXbwvQGUCOXzxK0KYhs195jwshdPW3js5J6RxG3Dyvj89zMCXsnlZsSwwbXvhipJDM+zrDWR9n/KL6LGkd4ogybNNmBVbRU2ofZWKcF/zs3285h+kdz5hrpZUdVLCHBHE45ZKmhjTSzrVbsxDYcodQkO4zmkW2c1R0pt+pR+f0oLgTtbTGFhN+OBP+Kdr4Hucw6DpApFvlyGuIf8Cim0YwOjwqu3C852maRGDcwpWW9oU0Rx+BkikUnc8Go4TsT1hnBWfQslTAQexALbisUXGErFxNWjL2sQi8sTiKBVIdYjYUDQ0yeoP0lxLiIhUhwYyF++hRPFNcW4yhMZUKHERNZt5cDAL4G6vPYFaG7oNQAAAAASUVORK5CYII="/>
<br>
<h1 class="report-title">
    BallySAC Registration Table Statistics
</h1>
<table id="tableData">
    <thead>
    <tr>
        <th>Date</th>
        <th >Count</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($data->userRegisterBetweenDates as $key => $item)
        <tr class="trTable">
            <td>{{ $item->date }}</td>
            <td>{{ $item->count }}</td>
        </tr>
    @endforeach
    <tr class="trTable" style="border-top: thin solid; border-bottom: thin solid; border-right: thin solid;">
        <td>Total</td>
        <td>{{ $data->totalSignupBetweenDates->total_user_signup }}</td>
    </tr>
    </tbody>
</table>
</body>
</html>
