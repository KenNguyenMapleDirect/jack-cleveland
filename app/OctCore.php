<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OctCore extends Model
{
    protected $table = 'October2021';
    public $timestamps = false;
    protected $primaryKey = 'BAC_Account';
    public $incrementing = false;
}

